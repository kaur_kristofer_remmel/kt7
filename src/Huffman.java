
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.*;


/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
class Node { //https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/

   int frequency;
   char character;

   Node leftNode;
   Node rightNode;

   @Override
   public String toString(){
      return character + ": " + frequency;
   }
}


class MyComparator implements Comparator<Node> { //https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
   public int compare(Node x, Node y)
   {

      return x.frequency - y.frequency; //väiksemad on ees
   }
}


public class Huffman {

   private byte[] original;
   private HashMap<Character, Integer> frequencyOfElements;
   private HashMap<Character, String> binaryMap;
   private Node rootNode = null;
   private Integer bitLenngth = null;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      this.frequencyOfElements = new HashMap<>();
      this.binaryMap = new HashMap<>();
      if (original == null){
         throw new RuntimeException("original code can not be null" + null);
      }
      this.original = original;

   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {

      return bitLenngth;


   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {

      String stringToEncode = new String(origData);

      findTheFrequencyOfTheElements(stringToEncode);

      PriorityQueue<Node> priorityQueue = new PriorityQueue<>(frequencyOfElements.size(), new MyComparator());

      for ( Map.Entry<Character, Integer> entry : frequencyOfElements.entrySet()){ //teen tipud ja panen nad priorityQueue'sse

         Node node = new Node();

         node.character = entry.getKey();
         node.frequency = entry.getValue();

         node.leftNode = null;
         node.rightNode = null;

         priorityQueue.add(node);
      }


      while (priorityQueue.size() > 1){

         Node nodeOne = priorityQueue.peek();
         priorityQueue.poll();

         Node nodeTwo = priorityQueue.peek();
         priorityQueue.poll();

         Node nodeParent = new Node();

         nodeParent.character = '-';
         nodeParent.frequency = nodeOne.frequency + nodeTwo.frequency;
         nodeParent.leftNode = nodeOne;
         nodeParent.rightNode = nodeTwo;

         priorityQueue.add(nodeParent);

      }

      String binaryCode = "";
      rootNode = priorityQueue.peek();
      makeBinaryHashMap(rootNode, binaryCode);

      System.out.println(binaryMap.toString());

      String code = lettersToBinary(stringToEncode);

      bitLenngth = code.length();

//      System.out.println(code);
//      byte[] bval = new BigInteger(code, 2).toByteArray();
//
//      System.out.println(new BigInteger(code, 2));

      return code.getBytes();
   }

   public void makeBinaryHashMap(Node root, String binaryCode){

      if (root.leftNode == null && root.rightNode == null && root.character != '-'){

         if (binaryCode.equals("")){
            binaryCode = "0";
         }

         binaryMap.put(root.character, binaryCode);

         return;
      }

      if (root.leftNode != null){
         makeBinaryHashMap(root.leftNode, binaryCode + '0');
      } else {
         throw new RuntimeException("root does not have a leftNode");
      }
      makeBinaryHashMap(root.rightNode, binaryCode + '1');
   }

   public String lettersToBinary(String str){

      StringBuilder newStr = new StringBuilder();

      char[] ch = str.toCharArray();

      for (char c : ch) {
         newStr.append(binaryMap.get(c));
      }

      return newStr.toString();
   }

   public void findTheFrequencyOfTheElements(String str){
      char[] charArray = str.toCharArray();

      for (char ch : charArray) {

         if (!frequencyOfElements.containsKey(ch)){
            frequencyOfElements.put(ch, 1);
         } else {
            frequencyOfElements.put(ch, frequencyOfElements.get(ch) + 1);
         }
      }
   }



   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {

      String encodedString = new String(encodedData);


      String letters = decodde(encodedString);


      System.out.println(letters);
      return letters.getBytes();
   }

   public String[] binaryToLetters(Node rootNode,String code, String binary){

      if (code.length() > 0){
         if (code.substring(0, 1).equals("1") && rootNode.rightNode != null){
            return binaryToLetters(rootNode.rightNode, code.substring(1), binary + "1");
         } else if (code.substring(0, 1).equals("0") && rootNode.leftNode != null){
            return binaryToLetters(rootNode.leftNode, code.substring(1), binary + "0");
         }
      }
      if (binary.equals("")){
         binary = "0";
      }

      return new String[] {Character.toString(rootNode.character), binary} ;
   }

   public String decodde(String code){

      StringBuilder letters = new StringBuilder();

      for (int i = 0; i < code.length(); i++) {

         String[] s = binaryToLetters(rootNode, code.substring(i), "");
         letters.append(s[0]);

         i =  i +  s[1].length() - 1;

      }
      return letters.toString();



   }






   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAABBBBCCCDFF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      System.out.println(new String(kood));
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      System.out.println("deez :" + orig.length);
      System.out.println("deez :" + kood.length);

   }
}

